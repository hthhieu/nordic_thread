FROM ubuntu:18.04

RUN apt-get update && apt-get install -y \
    build-essential vim git unzip \
    && rm -rf /var/lib/apt/lists/*

COPY nRF5SDKforThreadandZigbeev310c7c4730.zip /thread_sdk.zip

RUN mkdir -p /thread_sdk && \
    cd /thread_sdk && \
    unzip /thread_sdk.zip && \
    rm /thread_sdk.zip

COPY gcc-arm-none-eabi-7-2018-q2-update-linux.tar.bz2 /gcc.tar.bz2

RUN mkdir -p /usr/local/ && tar xvf /gcc.tar.bz2 -C /usr/local/ && \
    rm /gcc.tar.bz2

WORKDIR /thread_sdk

CMD /bin/bash
