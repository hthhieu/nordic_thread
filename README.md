# Introduction
Docker environment for developing Thread applications with Nordic's ICs.

# Build

``` shell
./get_sdk.sh
sudo docker build -t gochit/nordic_sdk:latest .
```

# Run

``` shell
sudo docker run -it gochit/nordic_sdk:latest
```
